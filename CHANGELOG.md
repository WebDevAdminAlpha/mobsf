# MobSF analyzer changelog

## v2.11.0
- Add an option to upload apk/ipa directly (!18)

## v2.10.0
- Update MobSF to version [3.4.3](https://github.com/MobSF/Mobile-Security-Framework-MobSF/releases/tag/v3.4.3) (!24)
  + Download and perform static analysis of third party apps from VM/AVD
  + Security Hardening
  + Android Dynamic Analysis TLS/SSL Security Tester
  + Refactored Logcat log viewer to show only app specific logs
  + New REST API exposed for TLS/SSL tests

## v2.9.0
- Remove need for external mobsf service (!23)

## v2.8.0
- Update MobSF to version [3.4.0](https://github.com/MobSF/Mobile-Security-Framework-MobSF/releases/tag/v3.4.0) (!21)
  + Android Hardcoded Secrets False Positive Improvement
  + New Android Crypto Rule
  + Rescan Fail-Safe and Code QA
  + Auto Comment for PR and Issues
  + USE_HOME by default
  + Dynamically Display Config Location
  + Fixed a bug in iOS ATS plist analysis
  + Removed Android Shared Library PIE Check
  + Improved Frida Instrumentation Logic to prevent Frida bypass
  + Fixed a False positive in Android Java Random rule
  + Fixed a bug that caused multiple first time saves of the same scan
  + Fixed Dynamic Analyzer JSON Report REST API bug

## v2.7.0
 - Update report dependency in order to use the report schema version 14.0.0 (!20)

## v2.6.0
- Update MobSF to version [3.3.3](https://github.com/MobSF/Mobile-Security-Framework-MobSF/releases/tag/v3.3.3) (!19)
  + Android Hardcoded Secrets Improvement
  + iOS IPA binary analysis improvements
  + Improved Android Manifest Analysis
  + Improved Setup
  + Updated to APKiD that is maintained by MobSF Team
  + Static Analysis Rule QA
  + macOS BigSur support
  + Update libsast to skip large files.
  + Improved iOS plist analysis
  + Relaxed Android Source code zip requirements
  + Fixed a bug in Android Shared Library RELRO check
  + Fixed a bug in Windows setup that prevents detection of python version on the first run
  + Fixed a bug in Recent Scan
  + Fixed a bug in root CA naming that prevented traffic interception

## v2.5.0
- Update MobSF to version 3.2.9 (!15)
  + Includes OWASP MSTG mappings in results: https://github.com/MobSF/Mobile-Security-Framework-MobSF/commit/e5107dc5918a76a0b8fa2c800475ad093746067c
  + Adds Support for Android 10 API 29 for Genymotion: https://github.com/MobSF/Mobile-Security-Framework-MobSF/commit/000dc362ab5bfc9d0d4d32a61dd093fb6e377be5
  + Fixes a template bug in the File Analysis Section of Android Source: https://github.com/MobSF/Mobile-Security-Framework-MobSF/pull/1640
  + xapk support https://github.com/MobSF/Mobile-Security-Framework-MobSF/commit/2648bf918a206066a33b891152fa36eb9c3acb21
  + Updates apktool to 2.5.0 https://github.com/MobSF/Mobile-Security-Framework-MobSF/commit/2417f1423f32d2ac3885f4b8ab7abb2688354e81
  + Allows insecure regex matches with kotlin https://github.com/MobSF/Mobile-Security-Framework-MobSF/commit/eef603bc4009b88dd183f2ddaed009c112c730c0
  + NIAP Analysis for Android: https://github.com/MobSF/Mobile-Security-Framework-MobSF/commit/d1f8f87d2d2a21c72223b7b39288a127c924d61f
  + Secret Detection: https://github.com/MobSF/Mobile-Security-Framework-MobSF/commit/81bd0aabda6f3a865ef30a263a959002745616f7

## v2.4.0
- Add Identifiers to Issue (!12)

## v2.3.0
- Update common to v2.22.0 (!11)
- Update urfave/cli to v2.3.0 (!11)

## v2.2.0
- Enable ruleset disablement (!9)

## v2.1.0
- Map severity `secure` to `severityLevel.Info` (!8)

## v2.0.0
- Finalize v2.0.0 release (!3)
- Mock project into MobSFs expected structure (!1)

## v1.0.0
- Initial beta release. Code provided by H-E-B (!2)
