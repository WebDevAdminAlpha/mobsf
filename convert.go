package main

import (
	"encoding/json"
	"fmt"
	"io"

	"gitlab.com/gitlab-org/security-products/analyzers/mobsf/v2/metadata"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v2"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"
)

func convert(reader io.Reader, prependPath string) (*report.Report, error) {
	newReport := new(Report)
	if err := json.NewDecoder(reader).Decode(newReport); err != nil {
		return nil, err
	}

	vulns := []report.Vulnerability{}
	for _, f := range newReport.Findings() {
		// The info findings are extremely high-volume and low-value.
		// So, we won't report them.
		if f.SeverityLevel() <= report.SeverityLevelInfo {
			continue
		}

		for _, loc := range f.Locations {
			v := report.Vulnerability{
				Category:    report.CategorySast,
				Scanner:     metadata.VulnerabilityScanner,
				Name:        f.ID,
				Message:     f.Description,
				Description: f.Description,
				Severity:    f.SeverityLevel(),
				Location:    loc.Location(),
				Identifiers: []report.Identifier{
					{
						Type:  "mobsf_rule_id",
						Name:  fmt.Sprintf("MobSF Rule ID %s", f.ID),
						Value: f.ID,
					},
				},
			}
			vulns = append(vulns, v)
		}
	}

	r := report.NewReport()
	r.Analyzer = metadata.AnalyzerID
	r.Config.Path = ruleset.PathSAST
	r.Vulnerabilities = vulns
	r.Scan.Scanner = metadata.ReportScanner
	return &r, nil
}
