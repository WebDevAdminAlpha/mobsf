package plugin

import (
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
)

var (
	manifestPath string
	// packagePath is the path to a apk, ipa, or other compiled mobile app packages
	packagePath    string
	analysisType   string
	uploadFilename string
)

const (
	// AnalysisTypeIOS is the identifier for iOS analysis
	AnalysisTypeIOS = "ios"
	// AnalysisTypeAndroid is the identifier for Android analysis
	AnalysisTypeAndroid = "android"
	// AnalysisTypePackageFile is the identifier for Android/iOS/UWP package file analysis
	AnalysisTypePackageFile = "packageFile"
	// Manifest is the file name of the Android manifest
	Manifest = "AndroidManifest.xml"
)

// Match checks if the file is AndroidManifest.xml or *.xcodeproj
func Match(path string, info os.FileInfo) (bool, error) {
	filename := info.Name()

	if IsSupportedPackageFileExtension(filepath.Ext(filename)) {
		analysisType = AnalysisTypePackageFile
		packagePath = path
		uploadFilename = filename
		return true, nil
	}

	// Don't change the analysis type if a package is already found
	if filepath.Ext(filename) == ".xcodeproj" && analysisType == "" {
		analysisType = AnalysisTypeIOS
		uploadFilename = "analyze.zip"
		return true, nil
	}

	if filename == Manifest && analysisType == "" {
		analysisType = AnalysisTypeAndroid
		manifestPath = path
		uploadFilename = "analyze.zip"
		return true, nil
	}

	return false, nil
}

// AnalysisType returns whether the type of analysis being performed is iOS or Android.
// It must be called after the Match stage.
func AnalysisType() string {
	log.Debugf("AnalysisType: %s", analysisType)
	return analysisType
}

// IsSupportedPackageFileExtension returns whether the package file extensions is supported to be uploaded directly.
func IsSupportedPackageFileExtension(extension string) bool {
	switch extension {
	case ".ipa", ".apk", ".appx":
		return true
	}
	return false
}

// ManifestPath returns the path to the Android manifest if AnalysisType is AnalysisTypeAndroid.
// It must be called after the Match stage.
func ManifestPath() string {
	return manifestPath
}


// PackagePath returns the path to the package file if AnalysisType is AnalysisTypePackageFile.
// It must be called after the Match stage.
func PackagePath() string {
	return packagePath
}

// UploadFilename returns the filename of the file to upload to MobSF.
// It must be called after the Match stage.
func UploadFilename() string {
	return uploadFilename
}

// IsMobsfFriendlyManifestPath returns true if the given path is "AndroidManifest.xml" or "app/src/main/AndroidManifest.xml"
func IsMobsfFriendlyManifestPath(path, analysisRoot string) bool {
	if path == filepath.Join(analysisRoot, Manifest) {
		return true
	}

	if path == filepath.Join(analysisRoot, "app", "src", "main", Manifest) {
		return true
	}

	return false
}

func init() {
	plugin.Register("mobsf", Match)
}
