ARG SCANNER_VERSION=3.4.3

FROM golang:1.15 as build

WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
  PATH_TO_MODULE=`go list -m` && \
  go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer

FROM opensecurity/mobile-security-framework-mobsf:v$SCANNER_VERSION

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION}

COPY entrypoint.sh /entrypoint.sh
COPY --from=build /analyzer /analyzer

ENTRYPOINT ["/entrypoint.sh"]
CMD ["/analyzer", "run"]
